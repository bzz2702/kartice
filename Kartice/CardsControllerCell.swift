//
//  CardsControllerCell.swift
//  Kartice
//
//  Created by Milan Banjanin on 02/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit

class CardsControllerCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    
}
