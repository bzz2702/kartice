//
//  SelectedCardController.swift
//  Kartice
//
//  Created by Milan Banjanin on 06/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit
import CoreData

class SelectedCardController: UIViewController {

    var barcode: String?
    var titleX: String?
    var notes: String?
    var cardX: NSManagedObject?
    var frontPicturePath: String?
    var backPicturePath: String?
    
    @IBOutlet weak var barcodeImage: UIImageView!
    @IBOutlet weak var barcodeNumber: UITextField!
    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var buttonsView: UIView!
    @IBOutlet weak var frontPictureImageView: UIImageView!
    @IBOutlet weak var backPictureImageView: UIImageView!
    
    
    @IBAction func cancel(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func save(_ sender: Any) {
        editDatabaseObject()
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
        barcodeNumber.text = barcode
        barcodeImage.image = Barcode.fromString(string: barcode!)
        title = titleX
        notesTextView.text = notes
        setupNotifications()
        notesTextView.delegate = self
        frontPictureImageView.image = fetchImage(with: frontPicturePath)
        backPictureImageView.image = fetchImage(with: backPicturePath)

    }
    
}

extension SelectedCardController: CardsControllerDelegate {
    func sendData(_ card: NSManagedObject) {
        
        cardX = card
        
        let x = card.value(forKey: "barcode") as! Int
        barcode = String(x)
        
        let y = card.value(forKey: "type") as! String
        titleX = y
        
        let z = card.value(forKey: "notes") as! String
        notes = z
        
        if let q = card.value(forKey: "cardImageFront") as? String {
            frontPicturePath = q
        }
        
        if let w = card.value(forKey: "cardImageBack") as? String {
          backPicturePath = w
        }
        
    }
    
}

extension SelectedCardController: UITextViewDelegate {
    
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(NewCardController.updateTextView(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NewCardController.updateTextView(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func updateTextView(notification: Notification)
    {
        if let userInfo = notification.userInfo
        {
            let keyboardFrameScreenCoordinates = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            
            let keyboardFrame = self.view.convert(keyboardFrameScreenCoordinates, to: view.window)
            
            if notification.name == UIResponder.keyboardWillHideNotification{
                view.frame.origin.y = 0
            }
            else {
                view.frame.origin.y = -keyboardFrame.height + (tabBarController?.tabBar.frame.height)! + buttonsView.frame.height
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            notes = textView.text
            return false
        }
        return true
    }
    
}

extension SelectedCardController {
    
    func editDatabaseObject() {
        
        guard
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
        else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Card", in: managedContext)!
        let batchUpdateRequest = NSBatchUpdateRequest(entity: entity)
        batchUpdateRequest.resultType = NSBatchUpdateRequestResultType.updatedObjectIDsResultType
        batchUpdateRequest.propertiesToUpdate = ["notes": notes!]
        
        do {
            try managedContext.execute(batchUpdateRequest)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        do {
            let result = try managedContext.execute(batchUpdateRequest) as? NSBatchUpdateResult
            let objectIDArray = result?.result as? [NSManagedObjectID]
            let changes = [NSUpdatedObjectsKey : objectIDArray]
            NSManagedObjectContext.mergeChanges(fromRemoteContextSave: changes as [AnyHashable : Any], into: [managedContext])
        } catch {
            fatalError("Failed to perform batch update: \(error)")
        }
    }
    
}

extension SelectedCardController {
    
    func fetchImage(with path: String?) -> UIImage? {
        guard
            let path = path
            else {
                return nil
        }
        let imagePath = NewCardController.shared.documentsPath.appendingPathComponent(path).path
        
        if let imageData = UIImage(contentsOfFile: imagePath) {
            return imageData
        } else {
            print("UIImage could not be created.")
            
            return nil
        }
    }
}

