//
//  CardsController.swift
//  Kartice
//
//  Created by Milan Banjanin on 02/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit
import CoreData

protocol CardsControllerDelegate: class {
    func sendData(_ card: NSManagedObject)
}

class CardsController: UIViewController, NewCardControllerDelegate {
    
    func addNewCard(_ card: NSManagedObject) {
        cards.append(card)
    }
    
    weak var delegate: CardsControllerDelegate?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var cards: [NSManagedObject] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        longPressSetup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
        guard
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
        else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Card")
        
        do {
            cards = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        
        collectionView.reloadData() // added so that newly added card is shown in the collection view. not a good solution, but couldn't think of other.

    }

}

extension CardsController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell: CardsControllerCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardsControllerCell", for: indexPath) as! CardsControllerCell
        
        let card = cards[indexPath.row]
        cell.label.text = card.value(forKey: "name") as? String
        
        let type = card.value(forKey: "type") as! String
        cell.imageView.image = UIImage(named: type)

        return cell
        
    }
    
}

extension CardsController: UICollectionViewDelegate, UIGestureRecognizerDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "SelectedCardController") as? SelectedCardController {
            show(vc, sender: self)
            delegate = vc
            delegate?.sendData(cards[indexPath.row])
            
        }
    }
    
    @objc func handleLongPress(gesture : UILongPressGestureRecognizer!) {
        if gesture.state != .ended {
            let p = gesture.location(in: self.collectionView)
            
            if let indexPath = self.collectionView.indexPathForItem(at: p) {
                
                let alert = UIAlertController(title: "Delete?", message: nil, preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
                let deleteAction = UIAlertAction(title: "Delete", style: .destructive) {
                    [unowned self] action in
                    self.removeFromDatabase(position: indexPath.row)
                }
                
                alert.addAction(deleteAction)
                alert.addAction(cancelAction)
                present(alert, animated: true)
                
            } else {
                return
            }
        }
    }
}

extension CardsController {
    
    func removeFromDatabase(position: Int) {
        
        guard
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
        else {
            return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        managedContext.delete(cards[position])
        cards.remove(at: position)
        
        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        collectionView.reloadData()
    
    }
}

extension CardsController {
    
    func longPressSetup() {
        let lpgr : UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(CardsController.handleLongPress))
        lpgr.minimumPressDuration = 0.5
        lpgr.delegate = self
        lpgr.delaysTouchesBegan = true
        self.collectionView?.addGestureRecognizer(lpgr)
    }
    
}
