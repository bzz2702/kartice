//
//  Barcode.swift
//  Kartice
//
//  Created by Milan Banjanin on 04/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit
import CoreImage

class Barcode {
    class func fromString(string: String) -> UIImage? {
        guard
            let data = string.data(using: .isoLatin1),
            let filter = CIFilter(name: "CICode128BarcodeGenerator")
            else { return nil }
        filter.setValue(data, forKey: "inputMessage")
        
        guard let image = filter.outputImage
            else { return nil }
        let size = image.extent.integral
        let output = CGSize(width: 250, height: 200)
        let matrix = CGAffineTransform(scaleX: output.width / size.width, y: output.height / size.height)
        UIGraphicsBeginImageContextWithOptions(output, false, 0)
        defer { UIGraphicsEndImageContext() }
        UIImage(ciImage: image.transformed(by: matrix))
            .draw(in: CGRect(origin: .zero, size: output))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
