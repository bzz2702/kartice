//
//  Card.swift
//  Kartice
//
//  Created by Milan Banjanin on 03/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import Foundation

enum CardType: String {
    case vulkan = "Vulkan"
    case mol = "Mol"
    case hm = "H&M"
    case zara = "Zara"
    case laguna = "Laguna"
    case nis = "NIS"
    case mozzart = "Mozzart"
    case meridian = "Meridian Bet"
    // itd.
    
}
