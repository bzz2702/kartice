//
//  NewCardController.swift
//  Kartice
//
//  Created by Milan Banjanin on 03/03/2019.
//  Copyright © 2019 Milan Banjanin. All rights reserved.
//

import UIKit
import BarcodeScanner
import CoreData

protocol NewCardControllerDelegate: class {
    func addNewCard(_ card: NSManagedObject)
}

class NewCardController: UIViewController {
    
    static let shared = NewCardController()
    let fileManager = FileManager.default
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    
    var cardX: NSManagedObject?
    
    weak var delegate: NewCardControllerDelegate?
    
    var currentImageView: UIImageView?
    
    @IBAction func openScanner(_ sender: Any) {
        instantiateBarcodeVC()
    }
    
    @IBAction func cancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func save(_ sender: Any) {
        
        guard let name = name
        else {
            let alert = UIAlertController(title: "Missing type/name!", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
            return
        }
        guard let barcode = barcode
        else {
            let alert = UIAlertController(title: "Missing barcode!", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            present(alert, animated: true)
            return
        }
        
        delegate = presentedViewController as? CardsController
        self.dismiss(animated: true, completion: nil)
        
        
        saveToDatabase(name: name, barcode: barcode, type: type!, notes: notes)
        
        delegate?.addNewCard(cardX!)
    }
    
    @IBAction func takeFrontPicture(_ sender: Any) {
        takePicture()
        currentImageView = frontPicture
    }
    
    @IBAction func takeBackPicture(_ sender: Any) {
        takePicture()
        currentImageView = backPicture
    }
    
    @IBOutlet weak var typePicker: UIPickerView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var barcodeImage: UIImageView!
    @IBOutlet weak var clickToScan: UIButton!
    @IBOutlet weak var barcodeLabel: UILabel!
    @IBOutlet weak var frontPicture: UIImageView!
    @IBOutlet weak var backPicture: UIImageView!
    @IBOutlet weak var frontPictureButton: UIButton!
    @IBOutlet weak var backPictureButton: UIButton!
    @IBOutlet weak var notesTextView: UITextView!
    @IBOutlet weak var buttonsView: UIView!
    
    var notes: String = ""
    
    var type: CardType? = CardType.laguna
    
    var availableTypes: [CardType] =
        [
        CardType.laguna,
        CardType.hm,
        CardType.meridian,
        CardType.mol,
        CardType.mozzart,
        CardType.nis,
        CardType.vulkan,
        CardType.zara
        ]

    var name: String? {
        didSet {
            nameTextField.text = name
        }
    }
    
    var barcode: Int?
    
    var frontPicturePath: String?
    var backPicturePath: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        typePicker.delegate = self
        typePicker.dataSource = self
        notesTextView.delegate = self
        setupNotifications()
        name = CardType.laguna.rawValue
    }
    
}

extension NewCardController {
    func setupNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(NewCardController.updateTextView(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NewCardController.updateTextView(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

extension NewCardController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return availableTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        name = availableTypes[row].rawValue
        type = availableTypes[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return availableTypes[row].rawValue
    }
}

extension NewCardController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            notes = textView.text
            return false
        }
        return true
    }
    
    @objc func updateTextView(notification: Notification)
    {
        if let userInfo = notification.userInfo
        {
            let keyboardFrameScreenCoordinates = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
            
            let keyboardFrame = self.view.convert(keyboardFrameScreenCoordinates, to: view.window)
            
            if notification.name == UIResponder.keyboardWillHideNotification{
                view.frame.origin.y = 0
            }
            else {
                view.frame.origin.y = -keyboardFrame.height + buttonsView.frame.height
            }
        }
    }
    
}

extension NewCardController: BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate {
    
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {

        let img = Barcode.fromString(string: code)
        barcodeImage.image = img
        barcodeImage.alpha = 1
        clickToScan.isUserInteractionEnabled = false
        clickToScan.titleLabel?.text = ""
        barcodeLabel.text = code
        barcode = Int(code)
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        // TBD
    }
    
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension NewCardController {
    func instantiateBarcodeVC() {
        let viewController = BarcodeScannerViewController()
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        present(viewController, animated: true, completion: nil)
    }

}

extension NewCardController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func takePicture() {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        
        switch currentImageView {
        case frontPicture:
            frontPicture.image = image
            frontPictureButton.titleLabel?.text = ""
            frontPicturePath = saveImage(image: frontPicture.image!)
        case backPicture:
            backPicture.image = image
            backPictureButton.titleLabel?.text = ""
            backPicturePath = saveImage(image: backPicture.image!)
        default:
            break
        }
        
    }
    
}

extension NewCardController {
    func saveToDatabase(name: String, barcode: Int, type: CardType, notes: String) {
        
        guard
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            else {
                return
        }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Card", in: managedContext)!
        let card = NSManagedObject(entity: entity, insertInto: managedContext)
        card.setValue(name, forKeyPath: "name")
        card.setValue(barcode, forKeyPath: "barcode")
        card.setValue(type.rawValue, forKeyPath: "type")
        card.setValue(notes, forKeyPath: "notes")
        card.setValue(frontPicturePath, forKey: "cardImageFront")
        card.setValue(backPicturePath, forKey: "cardImageBack")
        
        do {
            try managedContext.save()
            cardX = card
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
}

extension NewCardController {
    
    func saveImage(image: UIImage) -> String? {
        let date = String(Date.timeIntervalSinceReferenceDate)
        let imageName = date.replacingOccurrences(of: ".", with: "-") + ".png"
        
        if let imageData = image.pngData() {
            do {
                let filePath = documentsPath.appendingPathComponent(imageName)
                try imageData.write(to: filePath)
                return imageName
            }
            catch let error as NSError {
                print("\(imageName) cannot be saved: \(error)")
                return nil
            }
        } else {
            print("Could not convert UIImage to png data")
            return nil
        }
    }
}






